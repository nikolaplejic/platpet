from django.contrib import admin
from .models import Petition, Signature

from django.utils.translation import gettext as _
from django.utils.html import mark_safe
from django.urls import reverse


class PetitionAdmin(admin.ModelAdmin):
    list_display = ('name', 'frontend_link',)

    def frontend_link(self, obj):
        link = reverse('petitions:display', args=[obj.id])
        link_text = _("pregledaj")
        return mark_safe(
            '<a href="%s" target="blank">%s</a>' % (link, link_text,)
        )


admin.site.register(Petition, PetitionAdmin)
admin.site.register(Signature)
