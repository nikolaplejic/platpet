from django.core.mail import send_mail
from django.core import signing
from django.utils.translation import gettext as _
from django.template import Context, Template
from django.template.loader import get_template


def send_verification_email(url_root, petition, email):
    verification_object = {"email": email, "petition_id": petition.id}
    secret = signing.dumps(verification_object)
    email_content = {
        "url_root": url_root,
        "secret": secret,
        "petition": petition
    }

    email_tmpl_html = get_template('petitions/email-inlined.html')

    email_tmpl_plaintext = Template("""{% load i18n %}
    {% trans "Please confirm your signature for the petition" %} {{ petition.name }}.

    {% trans "To do so, please click on the following link" %}:
    {{ url_root }}{% url 'petitions:confirm' secret %}""")

    send_mail(
        _("Please confirm your signature for %s") % petition.name,
        email_tmpl_plaintext.render(Context(email_content)),
        "noreply@sudjeluj.org",  # from
        [email],                 # to
        html_message=email_tmpl_html.render(email_content)
    )
    return secret
