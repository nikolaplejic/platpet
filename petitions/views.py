import csv
import os

from django.http import \
    HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.core import signing
from django.contrib import messages
from django.utils.translation import gettext as _
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt

from .models import Petition, Signature
from .forms import SignatureForm
from .email import send_verification_email


def index(request):
    petitions = Petition.objects.order_by("-id")
    return HttpResponse(petitions)


@csrf_exempt
@xframe_options_exempt
def display(request, petition_id):
    petition = get_object_or_404(Petition, pk=petition_id)
    message = ""

    if request.method == "POST":
        signature_form = SignatureForm(request.POST)

        if signature_form.is_valid():
            if signature_form.cleaned_data["petition"].id != petition_id:
                raise Exception("Nope.")

            new_signature = signature_form.save(commit=False)

            if petition.requires_confirmation:
                new_signature.is_confirmed = False
                secret = send_verification_email(
                    os.getenv("PLATPET_HOSTNAME",
                              "https://peticije.sudjeluj.org"),
                    petition,
                    new_signature.email
                )
                new_signature.confirmation_secret = secret
                message = _("Petition successfully signed. Please check "
                            + "your inbox for the link to confirm your "
                            + "signature.")
            else:
                message = _("Petition successfully signed. Thanks!")

            new_signature.save()

            signature_form = SignatureForm(initial={"petition": petition})
    else:
        signature_form = SignatureForm(initial={"petition": petition})

    context = {
        "petition": petition,
        "signature_form": signature_form,
        "message": message,
    }
    return HttpResponse(render(request, 'petitions/display.html', context))


def embed(request, petition_id):
    # assume only petition administrators will be authenticated...
    if not request.user.is_authenticated:
        return HttpResponseForbidden()

    context = {
        "petition_id": petition_id
    }

    return render(request,
                  'petitions/embed.html',
                  context,
                  content_type="text/plain")


def signatures(request, petition_id):
    if not request.user.is_authenticated:
        return HttpResponseForbidden()

    petition = get_object_or_404(Petition, pk=petition_id)
    signatures = Signature.objects.filter(petition_id=petition_id,
                                          is_confirmed=True)

    context = {
        "petition": petition,
        "signatures": signatures,
    }
    return HttpResponse(render(request, 'petitions/signatures.html', context))


def signatures_csv(request, petition_id):
    if not request.user.is_authenticated:
        return HttpResponseForbidden()

    signatures = Signature.objects.filter(petition_id=petition_id,
                                          is_confirmed=True)

    response = HttpResponse(content_type='text/csv')

    writer = csv.writer(response)
    for signature in signatures:
        writer.writerow([
            signature.name,
            signature.city,
            signature.email,
            signature.contact_consent
        ])

    return response


def confirm_signature(request, secret):
    verification_object = signing.loads(secret)
    petition = get_object_or_404(Petition,
                                 pk=verification_object["petition_id"])
    signature = Signature.objects.get(petition_id=petition.id,
                                      email=verification_object["email"])

    signature.is_confirmed = True
    signature.save()
    return HttpResponseRedirect(reverse('petitions:thanks'))


def thanks(request):
    return HttpResponse(_("Thanks for confirming"))
