from django.urls import path

from . import views

app_name = 'petitions'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:petition_id>/', views.display, name='display'),
    path('<int:petition_id>/embed', views.embed, name='embed'),
    path('<int:petition_id>/signatures', views.signatures, name='signatures'),
    path('<int:petition_id>/signatures.csv',
         views.signatures_csv,
         name='signatures_csv'),
    path('confirm/<str:secret>', views.confirm_signature, name='confirm'),
    path('thanks', views.thanks, name='thanks'),
]
