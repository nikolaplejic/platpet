from django.db import models


class Petition(models.Model):
    name = models.CharField(max_length=400)
    description = models.TextField()
    requires_confirmation = models.BooleanField(default=True)
    consent_text = models.TextField(default="", blank=True)

    def __str__(self):
        return "%i: %s" % (self.id, self.name)


class Signature(models.Model):
    petition = models.ForeignKey(Petition, on_delete=models.CASCADE)
    name = models.CharField(max_length=400)
    city = models.CharField(max_length=150)
    email = models.EmailField()
    contact_consent = models.BooleanField(default=False, db_index=True)
    is_confirmed = models.BooleanField(default=True, db_index=True)
    confirmation_secret = models.CharField(max_length=400, null=True)

    class Meta:
        unique_together = ["petition_id", "email"]
