from django.forms import ModelForm, HiddenInput, CheckboxInput
from django.utils.translation import gettext as _
from django.utils.safestring import mark_safe

from .models import Signature


consent_label = mark_safe(_('I consent to my email address being stored and used to send further information on the activities of the Platform Možemo! More information is available in the <a href="https://www.mozemo.hr/privacy-policy/">Privacy Policy</a>.'))


class SignatureForm(ModelForm):
    class Meta:
        model = Signature
        fields = ['petition', 'name', 'city', 'email', 'contact_consent']
        widgets = {
            'petition': HiddenInput(),
        }
        labels = {
            "name": _("Name"),
            "city": _("City"),
            "email": _("Email Address"),
            "contact_consent": consent_label
        }

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        if 'petition' in self.initial \
           and self.initial['petition'].consent_text != "":
            consent_text = mark_safe(self.initial['petition'].consent_text)
        else:
            consent_text = consent_label

        self.fields['contact_consent'].label = consent_text
