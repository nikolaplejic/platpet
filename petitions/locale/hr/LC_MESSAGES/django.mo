��          �      ,      �     �     �     �  �   �     �  ]   �  %   �       =   4  $   r  .   �     �     �  
   �     �  ,   �  �        �     �     �  �   �     �  b   �  $     #   +  6   O  )   �  /   �     �     �     �     �  A   
                                                           	      
             City Confirm signature Email Address I consent to my email address being stored and used to send further information on the activities of the Platform Možemo! More information is available in the <a href="https://www.mozemo.hr/privacy-policy/">Privacy Policy</a>. Name Petition successfully signed. Please check your inbox for the link to confirm your signature. Petition successfully signed. Thanks! Please confirm signature Please confirm your petition signature by clicking on a link. Please confirm your signature for %s Please confirm your signature for the petition Sign Submit Thank you! Thanks for confirming To do so, please click on the following link Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Grad Potvrdi potpis E-mail adresa Prihvaćam pohranjivanje moje e-mail adrese za potrebe slanja obavijesti o radu političke platforme Možemo! Saznaj više u <a href="https://www.mozemo.hr/privacy-policy/">Pravilima privatnosti</a>. Ime i prezime Peticija uspješno potpisana. Molimo provjerite svoj email inbox kako biste potvrdili svoj potpis. Peticija uspješno potpisana. Hvala! Molimo vas da potvrdite svoj potpis Molimo vas da potvrdite svoj potpis klikanjem na link. Molimo vas da potvrdite svoj potpis za %s Molimo vas da potvrdite svoj potpis za peticiju Potpiši Pošalji Hvala! Hvala na potvrdi Kako biste to napravili, molimo vas da kliknete na sljedeći link 