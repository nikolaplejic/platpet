# Platform Petitions

## Requirements

- Python >= 3.6
- libsqlite3

## Running

    virtualenv --no-site-packages --python=python3 .
    . bin/activate
    cp platpet/settings.example.py platpet/settings.py
    python manage.py migrate
    python manage.py runserver
