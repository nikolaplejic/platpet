# Priručnik za korištenje platformskog sustava za peticije

Platformski sustav za peticije omogućuje jednostavno stvaranje i objavu peticija
na stranicama platformi ZJN! i Možemo!.

Svaka objava se sastoji od dva koraka:

- stvaranje nove peticije kroz platformski sustav;
- objava peticije na web stranicama platforme kroz WordPress.

Prije korištenja sustava, morate dobiti korisničko ime i lozinku. Podatke možete
zatražiti od Nikole Plejića emailom na nikola@plejic.com.

Sve upite oko korištenja možete ostaviti na [intranetskim stranicama radne grupe
za IT](https://platforma.sudjeluj.org/c/radne-grupe/grupa-za-it/47).

## Stvaranje peticije

Za stvaranje peticije, ulogirajte se u administracijski sustav na
[https://peticije.sudjeluj.org/admin/](https://peticije.sudjeluj.org/admin/):

<img src="01_login.png" style="max-width: 50%;" />

Na početnom ekranu, odaberite link "Novi unos" uz opciju "Petitions":

<img src="02_main.png" style="max-width: 50%;" />

Ispunite formular koji se prikaže na ekranu:

<img src="03_form.png" style="max-width: 50%;" />

Polja su:

- **Name:** naslov peticije;
- **Description:** kratki opis peticije;
- **Requires confirmation:** zahtijeva li peticija potvrdu potpisa putem emaila?
- **Consent text:** tekst privole za potrebe zakona o zaštiti osobnih podataka.

U slučaju nedoumica, polja **Requires confirmation** i **Consent text** možete
ostaviti praznima jer oba polja imaju razumne zadane postavke.

Klikom na "SPREMI" spremate peticiju i dolazite na popis svih dostupnih peticija
u platformi:

<img src="04_list.png" style="max-width: 50%;" />

Klikom na "pregledaj" uz peticiju, otvara se novi prozor s pregledom samog
formulara za ispunjavanje peticije:

<img src="05_petition.png" style="max-width: 50%;" />

Ovime je peticija spremna za objavu.

## Objava na stranicama mozemo.hr

Objavljivanje na našim WordPress siteovima započinje klikom na "Embed kod" na
formularu na kojem smo stali u prethodnom koraku. Otvorit će se novi prozor s
kodom kojeg ćemo za sada ostaviti po strani:

<img src="06_embed.png" style="max-width: 50%;" />

Potom ćemo se ulogirati u WordPress sustav kojeg želimo administrirati:

<img src="07_wplogin.png" style="max-width: 50%;" />

...te u Dashboardu odabrati "Stranice", pa "Dodaj novu"

<img src="08_wpdash.png" style="max-width: 50%;" />

...nakon čega će se otvoriti ekran za izradu nove stranice. Upišite naslov,
kliknite na "Pretpregled":

<img src="09_pageedit.png" style="max-width: 50%;" />

...pa na "Edit with WPBakery Page Builder". Oblikujte stranicu prema željama i
potrebama, a za ubacivanje formulara za peticiju, kliknite na "Add element"

<img src="10_wpbakery.png" style="max-width: 50%;" />

...te u popisu pronađite "Raw HTML"

<img src="11_rawhtml.png" style="max-width: 50%;" />

Pronađite tab iz koraka s početka u kojem se nalazi "embed kod". Kopirajte
cijeli kod, te ga zalijepite u "Raw HTML Settings" dijalog:

<img src="12_rawhtml_embed.png" style="max-width: 50%;" />

...te kliknite "Save changes".

Stranica je sad spremna za objavu klikom na "Publish" u gornjem-desnom kutu!

## Pregled potpisa peticije

Za pregled potpisa, ulogirajte se u administracijsko sučelje sustava za
peticije:

<img src="02_main_b.png" style="max-width: 50%;" />

Odaberite "Petitions", te na popisu peticija pronađite svoju:

<img src="04_list.png" style="max-width: 50%;" />

Kliknite na "pregledaj" uz peticiju, te na ekranu koji vam se otvori na "Vidi
potpise":

<img src="05_petition_b.png" style="max-width: 50%;" />

Popis potpisa će se prikazati u novoj stranici na kojoj ga možete i izvući u CSV
formatu.
